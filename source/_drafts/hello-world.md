---
title: Hello World
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).


## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

<!-- more -->

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/one-command-deployment.html)


https://github.com/next-theme/theme-next-docs
https://blog.cugxuan.cn/categories/
https://theme-next.js.org/
https://sspai.com/post/59337
https://www.qtmuniao.com/2019/10/16/hexo-theme-landscaping/
https://blog.typeart.cc/%E7%82%BA%E4%BB%80%E9%BA%BC%E6%88%91%E6%8E%A8%E8%96%A6hexo%E9%83%A8%E7%BD%B2%E5%88%B0Gitlab/
https://blog.typeart.cc/Git%E5%9F%BA%E7%A4%8E%E8%A8%AD%E5%AE%9A/
https://io-oi.me/tech/hexo-next-optimization/