---
title: CronJob at Laravel
date: 2021-05-02 17:34:55
tags: cronjob
categories: Laravel
---

## Introduction

- Laravel has come with a build-in **Task Scheduling** feature, it is served to control the range of excecution of each scheduler.

- However, the help of linux server's **crontab** is required to ensure the scheduler's artisan command is constantly excecuted.

<!-- more -->

## Documentation

- [Laravel Task Scheduling Doc](https://laravel.com/docs/6.x/scheduling)

- [CronTab Generator](https://crontab.guru/)

## How to implement

### 1. Scheduler

- Create a scheduler file in app\Console\Commands

```php
// app\Console\Commands\TestFoo.php (Example)

<?php

namespace App\Console\Commands;

use App\Http\Services\TestService;
use Illuminate\Console\Command;

class TestFoo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:test.foo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CronJob - Test Foo';

    protected $TestService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        TestService $TestService
    )
    {
        parent::__construct();
        $this->TestService = $TestService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        \Log::info('running Cron::test.foo');
        $this->TestService->test();
    }
}

```

- Register the command at app\Console\Kernel.php

```php
// app\Console\Commands\Kernel.php
 
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        
        $schedule->command('cron:test.foo')
            ->hourly();
    }

```

### 2. Crontab

- Excecute crontab with root user access <sup>#1</sup> , and select your favourite editor.

```bash
# update
sudo EDITOR=nano crontab -u root -e
```

- Insert the cronjob command, and save. (Crontab will auto-refreshed on saved.)

```
...
...
* * * * * cd <project_directory> && sudo php artisan schedule:run >> /dev/null 2>&1

```

- Double Check 

```bash
# list
sudo crontab -u root -l
```

## Remark
#1 - Crontab must be enable at root user access. To prevent future occurrences.