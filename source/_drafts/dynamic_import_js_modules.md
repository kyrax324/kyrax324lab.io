---
title: Dynamically require JS modules with Webpack 
description: 
date: 2021-05-02 17:34:55
tags: [vue2, webpack]
categories: ['Vue', 'LAVE']
---


### Introduction

When dealing with mulitple HTTP Request (API), I 

{% tabs downloading-next %}
<!-- tab Before -->

```js
import advertisementApi from './modules/AdvertisementApi'
import categoryApi from './modules/CategoryApi'
import commonApi from './modules/CommonApi'
import courseApi from './modules/CourseApi'
import courseEnrollmentApi from './modules/CourseEnrollmentApi'
import courseProviderApi from './modules/CourseProviderApi'
import currencyApi from './modules/CurrencyApi'
import enrollmentRatingApi from './modules/EnrollmentRatingApi'
import notificationApi from './modules/NotificationApi'
import qpointHistoryApi from './modules/QpointHistoryApi'
import refsApi from './modules/RefsApi'
import userApi from './modules/UserApi'
import voucherApi from './modules/VoucherApi'
import ReportApi from './modules/ReportApi'

const apiList = Object.assign(
    advertisementApi,
    categoryApi,
    commonApi,
    courseApi,
    courseEnrollmentApi,
    courseProviderApi,
    currencyApi,
    enrollmentRatingApi,
    notificationApi,
    qpointHistoryApi,
    refsApi,
    userApi,
    voucherApi,
    ReportApi,
)

export default apiList
```
<!-- endtab -->

<!-- tab After -->
```js
let api_collection = {}
let modules = require.context('./modules', false, /\.js$/)
modules.keys().map(path => {
    if (!path.includes('index.js')) {          
        let _module = modules(path).default || modules(path)
        api_collection =  Object.assign(api_collection, _module)
    }
})

const apiList = api_collection

export default apiList
```
<!-- endtab -->
{% endtabs %}

{% note primary %}
[Module Methods | webpack](https://webpack.js.org/api/module-methods/#requirecontext)
{% endnote %}
