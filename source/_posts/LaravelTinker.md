---
title: "Experience in Laravel Tinker"
description: 
date: 2021-07-15 13:44:00
categories: 
- [Laravel]
tags:
---

### Introduction

Laravel Tinker is a useful tool when developing with Laravel Framework.

It allows us to interact with our application, like Eloquent models, jobs, events, ... (even controllers) and provide more visualize on output of our code's logic.

### Usage

```bash
php artisan tinker
```

#### Dealing with Auth

We can initialize the auth instance by:

```php
auth()->loginUsingId(1)
```

#### Calling Contorllers & Services

```php
app()->call("App\Http\Controllers\SomeContorller@someMethod", [
	'someData' => 'someValue'
])
```

```php
app()->call("App\Http\Services\SomeService@someMethod", [
	'someData' => 'someValue'
])
```