---
title: "[Laravel] Usage of constant"
description: 
date: 2021-05-02 17:34:55
categories: 
- [Laravel]
tags:
---

### Introduction

.. Coming Soon ..
<!-- It's common to use some constant numbers to represent some types of an object. -->

```diff
// bad: hard to understand, require
- $user = User::where('type', 1)->get(); // 1 = Apple

// better: straight forward meaning
+ $user = User::where('type', User::APPLE)->get();
```

```php App\Models\User.php
	const APPLE = 1;
	const GOOLGE = 2;
	const FACEBOOK = 3;
	public const TYPES = [
		self::APPLE => "apple",
		self::GOOLGE => "google",
		self::FACEBOOK => "facebook",
	];
```