# Integrating CheckRole Middleware

##  Create 

Http\Middleware\CheckRole.php

```php
<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$params)
    {   
        $auth_user = auth()->user();

        // // check user.activation
        // if($auth_user->is_deleted){
        //     return response()->json([
        //         'status' => 401,
        //         'message' => 'Unauthorized'
        //     ], 401);
        // }

        // handle
        if(in_array($auth_user->role_id ,$params) ) {
            return $next($request);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Unauthorized'
        ], 403);
    }
}

```

Http\Kernel.php

```php

    protected $routeMiddleware = [
        // ...
        'checkRoles' => \App\Http\Middleware\CheckRole::class,
    ];

```

## Usage

routes/api.php

```php

Route::get('/somePath', [SomeController::class, 'someMethod'])->middleware(['checkRoles:1']); // only allow for Auth::user->role_id == 1
```