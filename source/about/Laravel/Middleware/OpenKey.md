# Integrating OpenKey Middleware

##  Create 

Http\Middleware\OpenKey.php

```php
<?php

namespace App\Http\Middleware;

use Closure;

class OpenKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$params)
    {   

        $access_key = $request->header('openkey');

        // todo: integrating with config()
        $available_access_key = [
            "someKey1",
            "someKey2",
        ];

        // handle
        if( $access_key && in_array($access_key, $available_access_key) ) {
            return $next($request);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Unauthorized Access Key'
        ], 403);
    }
}

```

Http\Kernel.php

```php

    protected $routeMiddleware = [
        // ...
        'openKey' => \App\Http\Middleware\OpenKey::class,
    ];

```

## Usage

routes/api.php

```php

Route::middleware('openKey')->group(function ()
{
    // ... some api route
});
```

## Integration with Swagger

register OpenKey at Controller.php

```
    * @OA\SecurityScheme(
    *   type="apiKey",
    *   in="header",
    *   securityScheme="open_key",
    *   name="OpenKey"
    * )
```

include OpenKey security

```

    /**
    * @OA\Post(
    *
    *   path="some path",
    *   operationId="some operationId",
    *   tags={"Some Tag"},
    *   summary="Some Summary",
    *   security={
    *       {
    *           "open_key": {}
    *       }
    *   },
    ...
    ...
    ...

```

## Integration with Axois

```js
window.axios.defaults.headers.common['OpenKey'] = 'someKey';
```