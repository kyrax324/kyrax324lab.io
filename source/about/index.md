---
title: Welcome to My Blog
#date: 2018-01-30 13:40:21
#description: NexT User Docs – Starting to Use
#categories: English
#lang: en
---

---

## About Me

Hello world, 
I am Software developer who are currently working on Laravel & Vue. 

Believe that the key to delivery high-quality code are:
- Readability
- Maintainability
- Changeability

Passioning in making significant contribution to the IT industries, check out my packages [here](/about/packages)

## Hobbies

- DIY / Crafts 
- Movies 🎞️
- Photography (🌇🌈)

## Coming Soon